import pickle;
import consts;
import cardList;
from cardList import dokkanCard;
import os;
from nameCorrector import correctName;
import colorama;
from colorama import Fore;
colorama.init(autoreset=True);
def correctString(stringer,options,correction):
    for i in range(0, len(options)):
        stre = stringer.lower();
        if(options[i] in stre):
            stringer = correction;
            return stringer;
    return -1;
def saveObj(obj, file):
    with open(file, "wb") as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL);
def loadObj(file):
    try:
        with open(file, "rb") as f:
            item = pickle.load(f);
            return item;
    except IOError:
        return 0;

class dokkanAccount(object):
    def __init__(self, region, stones, featuredCards, accountID, transfer):
        self.stones = stones;
        self.featuredCards = featuredCards;
        self.accountID = accountID;
        self.transfer = transfer;
        self.region = region;
def containsText(stringInput):
    return stringInput.upper().isupper();
    return any(i.isdigit() for i in stringInput); 
chooseEnd = False;
if(True):
    databaseExists = True;
    try:
        with open("cardsList.pkl") as f:
            databaseExists = True;
    except IOError:
        databaseExists = False;
        reAunth = False;
        
    if(databaseExists == True):
        
        cardsList = loadObj("cardsList.pkl");
        if("verify" in cardsList):
            if(len(cardsList) == cardsList["verify"]):
                redownload = input("Card list authenticated. Would you like to redownload card list?(y/n): ");
                cardsList.pop("verify", None);
                if(redownload == "y"):
                    cardsList = cardList.updateCardDatabase();
                    cardLen = len(cardsList)+1;
                    cardsList["verify"] = cardLen;
                    saveObj(cardsList,"cardsList.pkl");
                    cardsList.pop("verify", None)
            else:
                print("Card list is not authenticated and is likely corrupted. Search features may not work. Redownloading...");
                cardsList = cardList.updateCardDatabase();
                cardLen = len(cardsList)+1;
                cardsList["verify"] = cardLen;
                saveObj(cardsList,"cardsList.pkl");
                cardsList.pop("verify", None);
        else:
            print("Card list is not authenticated and is likely corrupted. Search features may not work. Redownloading...");
            cardsList = cardList.updateCardDatabase();
            cardLen = len(cardsList)+1;
            cardsList["verify"] = cardLen;
            saveObj(cardsList,"cardsList.pkl");
            cardsList.pop("verify", None);
        if(cardsList == 0):
            print("Error when fetching card list.");
            exit;
            
    else:
        print("Card database does not exist. Fetching now. This may take a while.");
        cardsList = cardList.updateCardDatabase();
        cardLen = len(cardsList)+1;
        cardsList["verify"] = cardLen;
        saveObj(cardsList,"cardsList.pkl");
        cardsList.pop("verify", None);
tempcardList = cardsList;
while(chooseEnd == False):
    cardsList = tempcardList;
    operations = ["add","search","edit","remove"];
    typeOpp = "";
    while(typeOpp not in operations):
        typeOpp = input("What would you like to do? Add an account, search for one or remove one?(add,search,edit,remove): ");
        if(typeOpp not in operations):
            print("Not a valid operation.")
    Operation = typeOpp.upper();
    print("What device type?");
    workingdir = os.getcwd();
    device = "";
    while(device!= "a" and device != "i"):
        device = input("'i'||IOS         'a'||Android");
        if(device == "a"):
            workingdir = os.path.join(workingdir,"accounts","android");
            if(os.path.exists(workingdir)):
                directory = workingdir;
                
            else:
                os.makedirs(workingdir);
                directory = workingdir;
        elif(device == "i"):
            workingdir = os.path.join(workingdir,"accounts","ios");
            if(os.path.exists(workingdir)):
                directory = workingdir;
                
            else:
                os.makedirs(workingdir);
                directory = workingdir;
        else:
            print("Not a valid device type.");
            
        
    if(Operation == "ADD"):
        defaults = input("Use defaults?(y/n): ");
        stones = 0;
        featuredCards = [];
        if(defaults == "n"):
            stones = "Not";
            while(containsText(stones)==True):
                stones = input("Number of stones in the account?");
                if(containsText(stones) == True):
                    print("Please input a number.");
            featuredCards = [-1];
            reset = False;
            while(any(x==-1 for x in featuredCards)):
                if(reset == False):
                    featuredCards = 0;
                    reset = True;
                featuredCards = input("Notable units?(Dbz.space ID or Name.Separate by commas): ");
                featuredCards =featuredCards.split(",");
                for x in range(0, len(featuredCards)):
                    if(containsText(featuredCards[x])):
                            featuredCards[x] = (correctName(featuredCards[x],cardsList.copy()));
                            if(featuredCards[x] != -1):
                                featuredCards[x] = featuredCards[x].strip();
                    else:
                        for t in cardsList:
                            if(cardsList[t].number == featuredCards[x]):
                               featuredCards[x] = t;
                               featuredCards[x] = featuredCards[x].strip();
                        if(containsText(str(featuredCards[x])) == False):
                               featuredCards[x] = -1;
                    if(featuredCards[x] == -1):
                        print("That is not a valid character. Try to be more accurate, or use the dbz.space id.");
        ID = "Not";
        while(containsText(ID) == True):
            ID = input("Account ID: ");
            if(containsText(ID)==True):
                print("Please input a number.");
        transfer = input("Account Transfer Code: ");
        region = "Not";
        regioners = ["g", "j"];
        while(region not in regioners):
            region = input("What region?(g,j): ");
            if(region not in regioners):
                print("Not a valid region.")
        fileExists = True;
        try:
            with open(directory + r"\accounts.pkl") as f:
                fileExists = True;
        except IOError:
            fileExists = False;
        account = dokkanAccount(region,stones,featuredCards,ID,transfer);
        if(fileExists == False):
            print("File does not exist. Creating...");
            
            objectSave = {"1":account};
            saveObj(objectSave,directory+ r"\accounts.pkl");
        else:
            file = loadObj(directory + r"\accounts.pkl");
            if(file != 0):
                newId = str(len(file)+1);
                file[newId] = account;
                saveObj(file,directory + r"\accounts.pkl");
                    

            else:
                print("Failed to read accounts.")
                break;
        del account;
    if(Operation == "SEARCH"):
        readit = loadObj(directory + r"\accounts.pkl");
        if(readit != 0):
            searchBy = [0,0,0,0];
            while(all(x==0 for x in searchBy)):
                searchCategories = input("What do you wish to search by?You can select multiple.Separate by commas.(stones,cards,accnum,region): ");
                searchCategoreis = searchCategories.split(",");
                searchTerms = ["stones","cards","accnum","region"];
                questions = ["Minimum number of Stones: ", "Featured Cards(Name, or dbz.space ID. Separate cards by commas): ", "Account Number: ", "Region: "];
                for i in range(0, len(searchCategories)):
                    searchString = searchCategories[i].lower();
                    for f in range(0,len(searchTerms)):
                        if(searchTerms[f] in searchString):
                                searchCategories[i] = searchTerms[f];
                for i in range(0, 4):
                    if(searchTerms[i] in searchCategories):
                        searchBy[i] = 1;
                searchAccount = [None,None,None,None];
                for i in range(0,4):
                    if(searchBy[3-i] == 1):
                        value = input(questions[3-i]);
                        searchAccount[i] = value;
                if(all(x==0 for x in searchBy)):
                    print("Not a valid search arguement.")
            print("Searching...");   
            accountList = readit;
            newAccountList = {};
            accountDetails = {};
            dictlen = len(accountList);
            accountCounter = 1;
            for i in range(1, dictlen+1):
                if(searchBy[3] == 1):
                    accessor = str(i);
                    regioner = correctString(searchAccount[0], ["jp","japan","jpan","j"],"j");
                    if(regioner == -1):
                        regioner = correctString(searchAccount[0], ["gl","global","glb","g"],"g");
                        if(regioner == -1):
                            print("That region is not valid.");
                            break;
                    if(accountList[accessor].region == regioner):
                        accountDetails[accessor] = accountList[accessor];
                        newAccountList[accessor] = accountList[accessor];
                    if(i==dictlen):
                        if(searchBy[2] == 0 and searchBy[1] == 0 and searchBy[0] == 0):
                            printString = [];
                            for s in accountDetails:
                                printString.append("Account Number: " + s + "\nAccount ID: " + accountDetails[s].accountID);
                            print(*printString, sep="\n");
            if(searchBy[0] == 1):
                accountDetails = {};
                if(searchBy[3] == 0):
                    newAccountList = readit;
                
                for u in newAccountList:
                    if(int(newAccountList[u].stones) >= int(searchAccount[3])):
                        accountDetails[u] = newAccountList[u];
                if(searchBy[2] == 0 and searchBy[1] == 0):
                        printString = [];
                        for t in accountDetails:
                            printString.append("Account Number: " + t + " Account ID: " + accountDetails[t].accountID + " Stones: " + accountDetails[t].stones);
                        if(printString != []):
                            print(*printString, sep="\n");
                        else:
                            print("No account found.")
                else:
                        popper = [];
                        for t in newAccountList:
                            if(t not in accountDetails):
                                popper.append(t);
                        for i in range(0,len(popper)):
                            newAccountList.pop(popper[i], None);
            if(searchBy[1] == 1):
                onlyParam = False;
                if(searchBy[2] == 0):
                    onlyParam = True;
                if(searchBy[0] == 0 and searchBy[3] == 0):
                    newAccountList = readit;
                searchCharacters = searchAccount[2].split(",");
                for a in range(0,len(searchCharacters)):
                    searchCharacters[a] = searchCharacters[a].strip();
                index = 0;
                printDetails = [];
                accountDetails = {};
                for a in searchCharacters:
                    if(containsText(a) == False):
                        for u in cardsList:
                            if(cardsList[u].number == a):
                                searchCharacters[index] = u;
                    else:
                        searchCharacters[index] = correctName(a,cardsList.copy());
                        
                        
                    index += 1;
                prevSearch = searchCharacters[:];
                for t in newAccountList:
                    searchCharacters = prevSearch;
                    accfeatChars = newAccountList[t].featuredCards;
                    for z in range(len(accfeatChars)):
                        for x in range(len(searchCharacters)):
                            if(searchCharacters[x] == accfeatChars[z]):
                                accountDetails[t] = newAccountList[t];
                    popper = [];
                    for s in accountDetails:
                        for u in searchCharacters:
                            if(u not in accountDetails[s].featuredCards):
                                popper.append(s);
                    for x in popper:
                        accountDetails.pop(x, None);
                if(onlyParam == True):
                    for t in accountDetails:
                        featCards = accountDetails[t].featuredCards
                        feater = "";
                        for i in featCards:
                            types = ["(Teq)", "(Agl)", "(Str)", "(Phy)", "(Int)"];
                            colours = [Fore.GREEN, Fore.BLUE, Fore.RED, Fore.YELLOW, Fore.MAGENTA];
                            for x in range (0,len(types)):
                                if(types[x] in i):
                                    i = i.replace(types[x], "");
                                    i = colours[x] + i;
                                    
                                
                            feater += i + ", ";
                        feater = feater[:-2];
                        
                            
                        printDetails.append("Account Number: " + t + " Account ID: " + accountDetails[t].accountID + " Featured Characters: " + feater);
                    print(*printDetails, sep= "\n");
                    if(len(accountDetails) == 0):
                        print("No account found.");
                    
                
                else:
                    if(len(newAccountList) != 0):
                        popper = [];

                        for x in newAccountList:
                            if(x not in accountDetails):
                                popper.append(x);
                        for i in range(0,len(popper)):
                            newAccountList.pop(popper[i]);

                        
                                    
                    
            
                
                
            if(searchBy[2] == 1):
                if(newAccountList == {} and searchBy[1] == 0 and searchBy[3]==0 and searchBy[0]==0):
                        newAccountList = readit;
                if(searchAccount[1] in newAccountList):
                    acc = newAccountList[searchAccount[1]];
                    feater = "";
                    types = ["(Teq)", "(Agl)", "(Str)", "(Phy)", "(Int)"];
                    colours = [Fore.GREEN, Fore.BLUE, Fore.RED, Fore.YELLOW, Fore.MAGENTA];
                    for u in acc.featuredCards:
                        for x in range (0,len(types)):
                            if(types[x] in u):
                                u = u.replace(types[x], "");
                                u = colours[x] + u;
                                    
                        feater += u + ", "
                    feater = feater[:-2];
                    print("Account Number: " + searchAccount[1] + "\nAccount ID: " + acc.accountID + "\nStones: "+ acc.stones + "\nFeatured Cards: " + feater + Fore.RESET + "\nRegion: " + acc.region);
                elif(i == dictlen):
                    print("No account with that number.");
                        
                    
               
                        
                    
                
            
            
            
        else:
            print("No accounts on this device.");
    elif(Operation == "REMOVE"):
        accountRemoveNumber = input("Please input the number of the account to remove: ");
        accountList = loadObj(directory + r"\accounts.pkl");
        if(accountList != 0):
            if(accountRemoveNumber in accountList):
                accountList.pop(accountRemoveNumber, None);
                if(int(accountRemoveNumber)<= len(accountList)):
                    lenner = len(accountList);
                    if(lenner != int(accountRemoveNumber)):
                        for i in range(int(accountRemoveNumber)+1,lenner+2):
                            print("Bruh");
                            accountList[str(i-1)] = accountList[str(i)];
                            accountList.pop(str(i), None);
                    else:
                       accountList[accountRemoveNumber] = accountList[str(lenner+1)];
                       accountList.pop(str(lenner+1));
                saveObj(accountList,directory + r"\accounts.pkl");
                print("Account with number " + accountRemoveNumber + " removed.");
        else:
            print("No account file exists.");
            break;
    elif(Operation == "EDIT"):
        accountEditNumber = input("Please input the number of the account to edit: ");
        accountList = loadObj(directory + r"\accounts.pkl");
        if(accountList != 0):
            if(str(accountEditNumber) in accountList):
                stoneNumber = "Brher";
                while(containsText(stoneNumber) == True):
                    stoneNumber = input("New amount of stones(Press Enter to leave unchanged): ");
                    if(containsText(stoneNumber)==True):
                        print("Please Input a number.");
                featuredCards = [-1];
                reset = 0;
                while(any(x==-1 for x in featuredCards)):
                    if(reset == 0):
                        featuredCards = [];
                        reset = 1;
                    featuredCards = input("New Featured Cards(Press enter to leave unchanged)(Dbz.space ID or Name, separate different cards by commas): ")
                    featuredCards = featuredCards.split(",");
                    for x in range(0,len(featuredCards)):
                        if(containsText(featuredCards[x]) == True):
                            featuredCards[x] = (correctName(featuredCards[x], cardsList.copy()));
                            if(featuredCards[x] != -1):
                                featuredCards[x] = featuredCards[x].strip();
                        else:
                            for z in cardsList:
                                if(featuredCards[x] == cardsList[z].number):
                                    featuredCards[x] = z;
                                    featuredCards[x] = featuredCards[x].strip();
                            if(containsText(str(featuredCards[x])) == False):
                                            featuredCards[x] = -1;
                    if(any(x==-1 for x in featuredCards)):
                        print("That is not  valid character. Please try and be more accurate.");
                            
                            
                transferer = input("New Transfer Code(Press enter to leave unchanged): ")
                if(stoneNumber != ""):
                    accountList[accountEditNumber].stones = stoneNumber;
                if(featuredCards != ""):
                    accountList[accountEditNumber].featuredCards = featuredCards;
                if(transferer != ""):
                    accountList[accountEditNumber].transfer = transferer;
                saver = saveObj(accountList,directory + r"\accounts.pkl");

            else:
                print("No account with that number exists.");
                
        else:
            print("No accounts of that device type on this device.")
            
        
                
                         
            
    
