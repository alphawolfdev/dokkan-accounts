Summary of Program:
----------------------
This program creates a database of accounts, each with certain characteristics, and allows you to manage and sort them quickly. The game has over
2000 individual named cards, and creating a search algorithm for every one would be unrealistic, so it also downloads a list of the cards from
another website: dbz.space. The possible characteristics of each account are: Dragon Stones(In game currency), "Featured Cards", "Account ID"(In Game ID),
and "Transfer Code"(A code used to transfer accounts between devices).

Usage Instructions:
-----------------
1.) Upon first launching the file(updateDatabase.exe), a download will begin immediately. This is the program fetching a card list from 
dbz.space. Upon subsequent launches, the download becomes optional as long as "cardsList.pkl" exists within the root directory.
(permissions may need to be granted).


2.) Once the download is complete, an input asking for a choice of four options will show(add, search, edit, delete).
	Add: Adds a new account with certain characteristics to the database.

	Search: Searches for an account(s) with certain characteristics. Parameters can be any combination of four. Amount of Dragon Stones(currency)
		,featured characters, account number and region(accounts can be global or japanese). 
	
	Edit: Allows you to edit an account and change the main values(region cannot be changed.).

	Remove: allows you to remove an account by it's account number.

Adding Characters:
-------------------
Characters can be added, searched for or edited in two ways. One of them is using a dbz.space Id. This is a unique ID for each card which can
be easily seen at dbz.space/cards/ and setting the "display" parameter to Card ID. The second method is by using a Name close to or that is
the name of the card, which can once again be obtained from dbz.space/cards/. The card list, once downloaded, is saved in the file cardsList.pkl
and is used to compare any inputted names against, with the most suitable being chosen.

Other Details:
--------------
Google Chrome needs to be installed on the PC, as this cannot be bundled with the program, or the card list cannot be downloaded.
All of this code was developed by me independently, so there may be small caveats which are unavoidable. Databases for the accounts are stored in
rootfolderOFProgram\accounts\ios(orandroid)\accounts.pkl and can be freely viewed, edited from the text file should the need arise.
