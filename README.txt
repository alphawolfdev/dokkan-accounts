This is the first stable build of my Dokkan Battle Database program. It is written in Python, and bundled using pyinstaller. 
All dependencies come bundled with the exe, so there should be no problem in that regard. Please refer to the "USAGE" file for instructions.
This app creates a pseudo-database of accounts for a card-collecting game, and manages them. It also handles netowrking and data fetching.
It may need priviliges to access the network in order to download some data essential for the search funtion, but there is no malicious intent there,
I just havent' found a workaround.
All the source code should be included for review.