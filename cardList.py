from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait;
from selenium.webdriver.chrome.options import Options;
from bs4 import BeautifulSoup;
import re;
import os;
import sys;
from selenium.common.exceptions import ElementNotInteractableException;
from selenium.common.exceptions import ElementClickInterceptedException;
def pather(relative_path):
    try:
        base_path = sys._MEIPASS;
    except Exception:
        base_path = os.path.dirname(__file__);
    return os.path.join(base_path,relative_path);
class dokkanCard(object):
    def __init__(self,title,number,rarity):
        self.title = title;
        self.rarity = rarity;
        self.number = number;
def updateCardDatabase():
    print("Establishing connection with dbz.space. Please wait...")
    cr_options = Options();
    cr_options.add_argument("--headless")
    urlPull = "https://dbz.space/cards";
    driver = webdriver.Chrome(pather("./chromedriver.exe"),options = cr_options);
    driver.get(urlPull);
    driver.implicitly_wait(20);



    wait = 0;
    load_more = driver.find_elements_by_xpath("//div[contains(@set, 'cards')]");
    print("Downloading card database from dbz.space. Warning, this can take a while. Allow up to five minutes.");
    increasetime = input("Set a delay time(1,2,5,10,100). The higher the number, the lower the wait. However, if internet is insufficient the page won't load, so choose a lower number.");
    check = int(increasetime);
    wait = int(increasetime);
    download = 0;
    while(len(load_more) != 0):
        while(check < 5000 + int(increasetime)-1):
            if(check >= 4999):
                isDownload = True;
                try:
                    load_more[0].click();
                except ElementNotInteractableException:
                    check = 0;
                    isDownload = False;
                    print("No interact");
                except ElementClickInterceptedException:
                    check = 0;
                    isDownload = False;
                    print("Intercept");
                if(isDownload == True):
                    check += int(increasetime);
                    wait = int(increasetime);
                    download += 1;
                    print("Downloading: " + str(download));
                
            else:
                check += int(increasetime);
        check = 0;
        while(wait < 5000+int(increasetime)-1):
            if(wait >= 4999):
                load_more = driver.find_elements_by_xpath("//div[@set='cards'][@class='btn mat']");
                readert = driver.find_elements_by_xpath("//div[@set='cards'][@class='btn mat hidden']");
                if(len(readert) != 0):
                    load_more = [];
                wait += int(increasetime);
            else:
                wait += int(increasetime);


    print("All data downloaded.");            
    soup = BeautifulSoup(driver.page_source, 'html.parser');
    driver.quit();
    cards = soup.find_all('a',class_="ab")
    types = soup.find_all('div', attrs={"ele":True});
    cardNamesRaw = [];
    cardNumbersRaw = [];
    cardElementsRaw = [];
    cardTypesRaw = [];
    cardRar = soup.find_all("td",class_="rarity");
    cardRarityRaw = [];
    for u in cardRar:
        xer = u.find_all("i");
        for s in xer:
            cardRarityRaw.append(s.text);



    for a in cards:
        cardNamesRaw.append(a.get("title"));
        
    for a in types:
        cardElementsRaw.append(a.get("ele"));
        cardTypesRaw.append(a.get("type"));
        cardNumbersRaw.append(a.get("base"));

    cardList = {};
    exer = 0;
    for i in range (0, len(cardNamesRaw)):
        title = cardNamesRaw[i].split("-")[0];
        title = title.strip();
        number = cardNumbersRaw[i];
        element = cardElementsRaw[i];
        typer = cardTypesRaw[i];
        
        finalType = "";
        if(typer == "1"):
            finalType = "Super";
        else:
            finalType = "Extreme";
            
        if(element == "0"):
            finalType += " Agl";
        elif(element == "1"):
            finalType += " Teq";
        elif(element == "2"):
            finalType += " Int";
        elif(element == "3"):
            finalType += " Str";
        else:
            finalType += " Phy";
        namer = cardRarityRaw[i].upper();
        namer += cardNamesRaw[i].split("-")[1];
        namer = namer.strip();
        namer += " (" + finalType.split()[1] + ")";
        if(namer not in cardList):
            cardList[namer] = dokkanCard(title,number,finalType);
        else:
            namer += " (" + finalType.split()[1] + ")";
            cardList[namer] = dokkanCard(title,number,finalType);
        exer += 1;
    return cardList;
        
        
        
        
        
    

