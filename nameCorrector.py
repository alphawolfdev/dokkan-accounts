from cardList import dokkanCard;
import re;
import math;
def correctName(nameInput, options):
    abbrev = ["SSJ", "SSJ2", "SSJ3", "SSJ4", "SSG", "SSB", "UI", "Gobros", "KC"];
    correction = ["Super Saiyan", r"Super Saiyan 2", "Super Saiyan 3", "Super Saiyan 4", "Super Saiyan God", "Super Saiyan God SS", "Ultra Instinct", "Super Saiyan Gohan(Teen) and Super Saiyan Goten(Kid)", "Super Saiyan 2 Caulifla and Super Saiyan 2 Kale"];
    stripName = nameInput.strip();
    fullName = stripName.split();
    upperName = [];
    for t in fullName:
        upperName.append(t.upper());
    nameList = options;
    updatedNameList = nameList;
    rarity = "";
    nameOnly = "";
    searchType = "";
    types = ["Teq","Agl", "Str", "Phy", "Int"];
    for x in range(0,len(upperName)):
        if("LR" == upperName[x]):
            rarity = "LR";   
        elif("UR" == upperName[x]):
            rarity = "UR";
        elif("SSR" == upperName[x]):
            rarity = "SSR";
        elif("SR" == upperName[x]):
            rarity = "SR";
        elif("R" == upperName[x]):
            rarity = "R";
        elif("N" == upperName[x]):
            rarity = "N";
    for i in types:
        if(i.upper() in upperName):
           searchType = i;
    nameOnly = nameInput.upper().replace(rarity.upper(), "",1);
        
    nameOnly = nameOnly.upper().replace(searchType.upper(), "");
    nameOnly = nameOnly.strip();
    for t in range(0,len(abbrev)):
        nameOnly = nameOnly.upper().replace(abbrev[t], correction[t])
    nameSplit = nameOnly.split();
    for y in nameList:
        rareCard = y.split();
        if(rarity != ""):
            if(rarity == rareCard[0]):
                updatedNameList[y] = nameList[y];
        
        if(searchType != ""):            
            if(searchType in nameList[y].rarity):
                updatedNameList[y] = nameList[y];
    popper = [];
    for t in updatedNameList:
        if(rarity != ""):
            rareCardNew = t.split();
            if(rareCardNew[0] != rarity):
                popper.append(t);
        if(searchType != ""):
            if(searchType not in updatedNameList[t].rarity):
                popper.append(t);

    for u in popper:
        updatedNameList.pop(u, None);
    popper = [];
    finalNameList = {};
    for x in updatedNameList:
        bracketText = re.search(r"\((\w+)\)", x);
        if(bracketText != None):
            bracketText = bracketText.group(1).upper();
            if(bracketText in nameSplit or bracketText == searchType.upper()):
                finalNameList[x] = updatedNameList[x];
        for y in nameSplit:
            nameClean = x.upper().replace(rarity.upper(), "").strip();
            if(y in nameClean):
                finalNameList[x] = updatedNameList[x];
    if(finalNameList == {}):
        return -1;
    elif(len(finalNameList) > 1):
        for z in finalNameList:
            temp = z.replace("&", "and");
            temp = temp.upper().strip();
            typers = ["(AGL)", "(TEQ)", "(INT)", "(STR)", "(PHY)"];
            for s in typers:
                if(s in temp):
                    temp = temp.replace(s, "");
            temp = temp.replace("(","");
            temp = temp.replace(")", "");
            cardNameList = temp.split();
            for e in cardNameList:
                if(e == rarity):
                    cardNameList.remove(e);
            if(len(cardNameList) == 1):
                if(len(nameSplit) == 1):
                    if(cardNameList[0].upper() == nameSplit[0]):
                        return z;
            elif(len(cardNameList) > 1):
                counter = 0;
                prev_len = len(cardNameList);
                cardNameList = list(dict.fromkeys(cardNameList));
                prev_searchlen = len(nameSplit);
                nameSplit = list(dict.fromkeys(nameSplit));
                while(len(nameSplit) < prev_searchlen):
                    nameSplit.append("Redundant Element.");
                    
                while(len(cardNameList) < prev_len):
                    cardNameList.append("Redacted, Repeating Element");
                for h in cardNameList:
                    for u in nameSplit:
                        if(u.upper() == h.upper()):
                            counter += 1;
                if(counter >= math.ceil(len(cardNameList)/1.5) and len(cardNameList) >= len(nameSplit)):
                    return z;
        return -1;

    elif(len(finalNameList) == 1):
        for t in finalNameList:
            return t;
        
    
    
    
